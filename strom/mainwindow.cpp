#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QApplication>
#include <QDir>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // ui->treeWidget->header ()->hide ();

    QStringList labels { "name", "size", "date" };
    ui->treeWidget->setHeaderLabels (labels);

    // QList <QString> labels;
    // labels << "name" << "size" << "date" ;
    // ui->treeWidget->setHeaderLabels (labels);

    // ui->treeWidget->setHeaderLabels ( QStringList () <<"name" << "size" << "date" );

    ui->treeWidget->clear ();
    QTreeWidgetItem * root = ui->treeWidget->invisibleRootItem();

    // #include <QDir>
    QDir dir (".");
    // QStringList list = dir.entryList ();
    QList <QFileInfo> list = dir.entryInfoList ();
    for (QFileInfo info : list)
    {
        QTreeWidgetItem * item = new QTreeWidgetItem;
        item->setText (0, info.fileName ());
        item->setText (1, QString::number (info.size ())+ " ");
        item->setText (2, info.lastModified().toString("yyyy-MM-dd hh:mm:ss"));
        item->setTextAlignment (1, Qt::AlignRight);

        if (info.isDir())
            item->setForeground (0, QColor (255, 0, 0));
        else
            item->setForeground (0, QColor (0, 0, 255));

        item->setToolTip (0, info.absoluteFilePath());

        root->addChild (item);
    }

    /*
    QTreeWidgetItem * root = new QTreeWidgetItem;
    root->setText (0, "koren stromu");
    root->setForeground (0, QColor ("brown"));
    ui->treeWidget->addTopLevelItem (root);

    QTreeWidgetItem * branch = new QTreeWidgetItem;
    branch->setText (0, "vetev");
    branch->setForeground (0, QColor ("green"));
    root->addChild (branch);

    QStringList list = QColor::colorNames();
    for (QString name : list)
    {
        QTreeWidgetItem * item = new QTreeWidgetItem;
        item->setText (0, name);
        item->setForeground (0, QColor (name));
        branch->addChild (item);
    }
    */

    ui->treeWidget->expandAll();
}

MainWindow::~MainWindow()
{
    delete ui;
}

int main (int argc, char *argv[])
{
    QApplication a (argc, argv);
    QApplication::setStyle ("windows");
    MainWindow w;
    w.show();
    return a.exec();
}

