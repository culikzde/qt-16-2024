#ifndef LEXER_H
#define LEXER_H

#include "precompiled.h"
using namespace std;

/* ---------------------------------------------------------------------- */

typedef vector <double> Vec;
typedef vector <Vec> Mat;

Mat readMat (string fileName);
void writeMat (string fileName, const Mat mat);

/* ---------------------------------------------------------------------- */

enum TokenKind { ident, number, text, separator, eos };

class Lexer
{
private:
    ifstream f;
    char ch;
    void nextChar ();
    void digits ();

public:
    TokenKind kind;
    string token;
    void nextToken ();
    void error (string msg);

public:
    bool isSeparator (char c);
    void checkSeparator (char c);

    double readNumber ();

public:
    Lexer (string fileName);
    ~Lexer ();
};

#endif // LEXER_H

