QT += core gui widgets

CONFIG += c++11 precompile_header

PRECOMPILED_HEADER += precompiled.h

HEADERS += precompiled.h lexer.h simplex.h

SOURCES += lexer.cc simplex.cc

FORMS += simplex.ui
