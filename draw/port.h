#ifndef PORT_H
#define PORT_H

#include <QGraphicsRectItem>

class Source;
class Target;
class ConnectionLine;

class Common : public QGraphicsRectItem
{
public:
   Common ();

   #ifdef DRAG_AND_DROP
   void dragEnterEvent (QGraphicsSceneDragDropEvent * event) override;
   void dropEvent (QGraphicsSceneDragDropEvent * event) override;
   #endif
};

class Source : public Common
{
public:
   Source ();

   int rel = 0;
   Target * target = nullptr;
   ConnectionLine * line = nullptr;
};

class Target : public Common
{
public:
   Target ();

   int rel = 0;
   Source * source = nullptr;
   void updateLine ();
   QVariant itemChange (GraphicsItemChange change, const QVariant &value) override;
};

class ConnectionLine : public QGraphicsLineItem
{
public:
   ConnectionLine ();
};

Source * createConnection ();

#endif // PORT_H
