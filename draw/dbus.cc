
/* dbus.cc */

#include "dbus.h"
#include "io.h"

#include <QDBusConnection>
#include <QDBusInterface>

#include <QAbstractGraphicsShapeItem>

#include <iostream>
using namespace std;

/* ---------------------------------------------------------------------- */

Receiver::Receiver (QWidget * parent, QTextEdit * p_info, QGraphicsScene * p_scene) :
    QObject (parent),
    info (p_info),
    scene (p_scene)
{
    QDBusConnection bus = QDBusConnection::sessionBus();
    if (bus.isConnected ())
        if (bus.registerService ("org.example.receiver"))
            if (bus.registerObject ("/org/example/ReceiverObject", this, QDBusConnection::ExportAllSlots))
            {
                info->append ("DBus Receiver ready");

                for (QGraphicsItem * item : scene->items ())
                   if (QAbstractGraphicsShapeItem * box = dynamic_cast <QAbstractGraphicsShapeItem *> (item))
                   {
                       ShapeObject * obj = new ShapeObject (box);
                       QString name = obj->getName ();
                       // info->append ("shape " + name);
                       if (bus.registerObject ("/shape/" + name, obj, QDBusConnection::ExportAllContents))
                           info->append ("ready " + name);
                   }
            }
}

Receiver::~Receiver()
{
    // info->append ("DBus Receiver finished");
    cout << "DBus Receiver finished" << endl;
}

void Receiver::hello (QString s)
{
    info->append ("Receiver - method hello : " + s);
}

void Receiver::navigateToSlot (const QString & objectName, const QString & signalSignature, const QStringList & parameterNames)
{
    info->append ("Receiver - method navigateToSlot (" + objectName + ", " + signalSignature + ", [" + parameterNames.join (",") + "])" );
}

/* ---------------------------------------------------------------------- */

void callHello (QString msg)
{
    QDBusConnection bus = QDBusConnection::sessionBus();
    if (bus.isConnected ())
    {
        cout << "BUS" << endl;
        QDBusInterface ifc ("org.example.receiver", "/org/example/ReceiverObject", "org.example.ReceiverInterface", bus);
        if (ifc.isValid ())
        {
            cout << "INTERFACE" << endl;
            ifc.call ("hello", msg);
        }
    }
}

void callNavigateToSlot (const QString & objectName, const QString & signalSignature, const QStringList & parameterNames)
{
    QDBusConnection bus = QDBusConnection::sessionBus();
    if (bus.isConnected ())
    {
        cout << "BUS" << endl;
        QDBusInterface ifc ("org.example.receiver", "/org/example/ReceiverObject", "org.example.ReceiverInterface", bus);
        if (ifc.isValid())
        {
            cout << "INTERFACE" << endl;
            ifc.call ("navigateToSlot", objectName, signalSignature, parameterNames);
        }
    }
}

/* ---------------------------------------------------------------------- */

/*
  yum install qt-qdbusviewer
  qdbusviewer, vyhledat org.example.receiver

  nebo

  yum install qt5-qdbusviewer
  qdbusviewer-qt5
*/

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
