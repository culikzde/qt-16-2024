#include "tree.h"
#include "toolbutton.h"
#include "io.h"

#include <QPen>
#include <QAbstractGraphicsShapeItem>

// #include <iostream>
// using namespace std;

/* ---------------------------------------------------------------------- */

Tree::Tree (QWidget * parent) :
    QTreeWidget (parent),
    scene (nullptr),
    win (nullptr)
{
    header()->hide ();
    setAcceptDrops (true); // <-- important
    setDropIndicatorShown (true);
    // setDragDropMode (InternalMove);
}

/* ---------------------------------------------------------------------- */

QStringList Tree::mimeTypes() const // <-- importatnt
{
    return QStringList () << "application/x-color" << toolFormat;
}

Qt::DropActions Tree::supportedDropActions() const
{
   return Qt::MoveAction | Qt::CopyAction  | Qt::LinkAction;
}

bool Tree::dropMimeData (QTreeWidgetItem * target, int index, const QMimeData * data, Qt::DropAction action)
{
    bool result = false;
    TreeItem * node = dynamic_cast < TreeItem * > (target);

    if (data->hasColor ())
    {
        QColor color = data->colorData().value <QColor> ();
        if (node != nullptr)
        {
           QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (node->graphics_item);
           if (shape != nullptr)
           {
               if (action == Qt::CopyAction) /* ctrl */
                  shape->setPen (color); // need #include <QPen>
               else
                  shape->setBrush (color);
           }
        }
        result = true;
    }
    else if (data->hasFormat (toolFormat))
    {
        QString tool = data->data (toolFormat);
        QGraphicsItem * item = itemFromTool (tool, 0, 0);
        if (item != nullptr)
        {
           if (node == nullptr)
               scene->addItem (item);
           else
               item->setParentItem (node->graphics_item);
        }
        // win->refreshNewTreeItem (item);
        win->refreshTree ();
    }

    return result;
}

/* ---------------------------------------------------------------------- */

inline QString str (qreal x)
{
    return QString::number (x);
}

void Tree::displayBranch (QTreeWidgetItem * target, QGraphicsItem * item)
{
    QColor color;
    QString name;

    if (QGraphicsLineItem * line = dynamic_cast <QGraphicsLineItem *> (item))
    {
        name = "line";
        color = line->pen ().color();
    }
    else if (auto shape = dynamic_cast <QAbstractGraphicsShapeItem *> (item))
    {
        name = "shape";
        color = shape->brush ().color();
        if (QGraphicsRectItem * r = dynamic_cast <QGraphicsRectItem *> (item))
        {
            QRectF rr = r->rect();
            name = "rectangle " + str (rr.width()) + " x " + str (rr.height());
        }
        else if (QGraphicsEllipseItem * e = dynamic_cast <QGraphicsEllipseItem *> (item))
        {
            name = "ellipse";
        }
    }

    if (color == QColor ("yellow"))
        color = QColor ("orange");

    QString t = item->toolTip();
    if (t != "")
        name = t;

    if (name == "")
       name = itemType (item);

 if (name == "")
       name = "node";

    TreeItem * node = new TreeItem;
    node->graphics_item = item;
    node->setText (0, name);
    node->setToolTip (0, t);
    node->setForeground (0, color);
    target->addChild (node);

    // bool opaque = item->data (opaqueKey).toBool ();
    // if (not opaque)
       for (QGraphicsItem * t : item->childItems ())
          displayBranch (node, t);

    if (target == nullptr)
       addTopLevelItem (node);
    else
       target->addChild (node);
}

void Tree::displayTree ()
{
    clear();

    QTreeWidgetItem * target = invisibleRootItem ();
    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
    {
        if (item->parentItem () == nullptr)
            displayBranch (target, item);
    }

    expandAll();
}

/* ---------------------------------------------------------------------- */

TreeItem * Tree::findTreeItem (QGraphicsItem * item)
{
    TreeItem * result = nullptr;
    if (item != nullptr)
    {
        QGraphicsItem * above = item->parentItem ();
        QTreeWidgetItem * branch = nullptr;
        if (above == nullptr)
        {
            branch = invisibleRootItem ();
        }
        else
        {
            branch = findTreeItem (above);
        }
        int cnt = branch->childCount ();
        for (int inx = 0; inx < cnt && result == nullptr; inx ++)
        {
           TreeItem * node = dynamic_cast <TreeItem *> (branch->child (inx));
           if (node != nullptr && node->graphics_item == item)
              result = node;
        }
    }
    return result;
}

void Tree::renameTreeItem (QGraphicsItem * item, QString name)
{
    /*
    QTreeWidgetItemIterator it (this);
    while (*it)
    {
       TreeItem * node = dynamic_cast < TreeItem * > (*it);
       if (node != nullptr && node->item == item)
       {
           node->setText (0, name);
       }
       it ++;
    }
    */


    /*
    TreeItem * node = findTreeItem (item);
    if (node != nullptr)
        node->setText (0, name);
    */
}

void Tree::addTreeItem (QGraphicsItem * item)
{
    QGraphicsItem * above = item->parentItem ();
    QTreeWidgetItem * branch = nullptr;
    if (above == nullptr)
        branch = invisibleRootItem ();
    else
        branch = findTreeItem (above);

    TreeItem * node = new TreeItem ();
    node->graphics_item = item;
    node->setText (0, item->toolTip ());
    branch->addChild (node);
}

/* ---------------------------------------------------------------------- */
