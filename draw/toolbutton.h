#ifndef TOOLBUTTON_H
#define TOOLBUTTON_H

#include <QToolButton>

class ToolButton : public QToolButton
{
public:
    ToolButton (QString p_name, QString p_format);
private:
    QString name;
    QString format;
    QPixmap image;
protected:
    void mousePressEvent (QMouseEvent *event) override;
};

#endif // TOOLBUTTON_H
