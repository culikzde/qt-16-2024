#include "port.h"
#include "block.h"
#include <QPen>
#include <QBrush>
#include <QMimeData>
#include <QGraphicsSceneDragDropEvent>

const int size = 12;

Common::Common ()
{
    setRect (0, 0, size, size);
    setFlags (QGraphicsItem::ItemIsMovable |
              QGraphicsItem::ItemIsSelectable |
              QGraphicsItem::ItemSendsScenePositionChanges);

    #ifdef DRAG_AND_DROP
    setAcceptDrops (true); // <--
    #endif
}

Source::Source ()
{
    setPen (QColor ("orange"));
    setBrush (QColor ("lime"));
}

Target::Target ()
{
    setPen (QColor ("orange"));
    setBrush (QColor ("yellow"));
}

ConnectionLine::ConnectionLine ()
{
    setPen (QColor ("red"));
}

#ifdef DRAG_AND_DROP
void Common::dragEnterEvent (QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * mime = event->mimeData ();
    if (mime->hasColor())
        setAcceptDrops (true);
    else
        setAcceptDrops (false);
}

void Common::dropEvent (QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * mime = event->mimeData ();
    if (mime->hasColor ())
    {
        modifyColor (this, event);
    }
}
#endif

void Target::updateLine()
{
    if (source != nullptr && source->line != nullptr)
    {
        QGraphicsLineItem * line = source->line;

        QPointF point (size/2, size/2);
        point = source->mapFromItem (this, point);
        line->setPos (size/2, size/2);
        line->setLine (0, 0, point.x()-size/2, point.y()-size/2);
    }

}

QVariant Target::itemChange (QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemScenePositionHasChanged)
    {
        updateLine ();
    }
    return QGraphicsRectItem::itemChange (change, value);
}

Source * createConnection ()
{
    Source * source = new Source;
    Target * target = new Target;
    ConnectionLine * line = new ConnectionLine;

    source->target = target;
    source->line = line;

    target->source = source;

    line->setParentItem (source);

    target->setParentItem (source);
    target->setPos (200, 100);
    target->updateLine ();

    return source;
}
