#include "block.h"
#include "circle.h"
#include "port.h"
#include "colorbutton.h"
#include "toolbutton.h"

#include <QPen>
#include <QBrush>
#include <QGraphicsSceneDragDropEvent>
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include <QGraphicsSceneDragDropEvent>

Block::Block(bool add_circles)
{
    setPos (80, 40);
    setRect (0, 0, 200, 100);
    setPen (QColor ("blue"));
    setBrush (QColor ("yellow"));
    setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);

    /*
    QPen p;
    p.setColor (QColor ("blue"));
    p.setWidth (3);
    r->setPen (p);
    */

    /*
    QLinearGradient b (0, 0, 200, 100);
    b.setStart (0, 0);
    b.setFinalStop (200, 100);
    b.setColorAt (0, QColor ("yellow"));
    b.setColorAt (1, QColor ("red"));
    r->setBrush (b);
    */

    if (add_circles)
    {
        setToolTip ("block");
        for (int i = 1; i <= 2; i ++)
        {
            Circle * e = new Circle;
            e->setPos (40 + (i-1) * 80, 30);
            e->setToolTip ("circle " + QString::number (i));
            e->setParentItem (this);

            /*
            QGraphicsEllipseItem * e = new QGraphicsEllipseItem;
            e->setPos (40 + (i-1) * 80, 30);
            e->setRect (0, 0, 40, 40);
            e->setToolTip ("circle " + QString::number (i));
            e->setPen (QColor ("blue"));
            e->setBrush (QColor ("cornflowerblue"));
            e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsFocusable);
            e->setParentItem (this);
            */
        }
    }

    #ifdef DRAG_AND_DROP
    setAcceptDrops (true); // <--
    #endif
}

#ifdef DRAG_AND_DROP
void modifyColor (QAbstractGraphicsShapeItem * shape,
                  QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * mime = event->mimeData ();

    // QColor color = mime->colorData().value <QColor> ();
    QColor color = qvariant_cast <QColor> (mime->colorData());

    if (event->proposedAction() == Qt::CopyAction) // ctrl
       shape->setPen (color);
    else
       shape->setBrush (color);
}

void addObject (QGraphicsItem * shape,
                QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * mime = event->mimeData ();
    QString name = mime->data (toolFormat);

    QPointF pos = event->scenePos () - shape->scenePos();

    if (name == "rectangle")
    {
        Block * obj = new Block;
        obj->setPos (pos);
        obj->setParentItem (shape);
    }
    else if (name == "ellipse")
    {
        Circle * obj = new Circle;
        obj->setPos (pos);
        obj->setParentItem (shape);
    }
    else if (name == "line")
    {
        Port * obj = createConnection ();
        obj->setPos (pos);
        obj->setParentItem (shape);
    }
}

void Block::dragEnterEvent (QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * mime = event->mimeData ();
    if (mime->hasColor () || mime->hasFormat (toolFormat))
       event->setAccepted (true);
    else
       event->setAccepted (false);
}

void Block::dropEvent (QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * mime = event->mimeData ();
    if (mime->hasColor ())
    {
        modifyColor (this, event);
    }
    else if (mime->hasFormat (toolFormat))
    {
        addObject (this, event);
    }
}
#endif

void Block::move (int x, int y)
{
    QPointF p = pos () + QPointF (x, y);
    setPos (p);
    p.setX(p.x() + x);
    p.setY(p.x() + x);
}

void Block::setColor (QString color)
{
    setBrush (QColor (color));
}

void Block::setBorder (QString color, int width)
{
    setPen (QPen (QColor (color), width));
}

