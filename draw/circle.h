#ifndef CIRCLE_H
#define CIRCLE_H

#include <QGraphicsEllipseItem>

class Circle : public QGraphicsEllipseItem
{
public:
    Circle();

    #ifdef DRAG_AND_DROP
    void dragEnterEvent (QGraphicsSceneDragDropEvent * event) override;
    void dropEvent (QGraphicsSceneDragDropEvent * event) override;
    #endif
};

#endif // CIRCLE_H
