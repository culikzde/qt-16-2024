#ifndef WIN_H
#define WIN_H

class QGraphicsItem;
class QGraphicsScene;

class QTextEdit;

class Tree;
class PropertyTable;

class Win // MainWindow interface
{
    public:
        virtual void showProperty (QString name, QVariant value) = 0;
        virtual void showProperties (QGraphicsItem * item) = 0;
        virtual void showInfo (QString s) = 0;

        virtual void refreshTree () = 0;
        virtual void refreshTreeName (QGraphicsItem * item, QString name) = 0;
        virtual void refreshNewTreeItem (QGraphicsItem * item) = 0;

        virtual QMainWindow * getMainWindow () = 0;
        virtual QStatusBar * getStatusBar () = 0;
        virtual Tree * getTree () = 0;
        virtual PropertyTable * getPropTable () = 0;
        virtual QTextEdit * getInfo () = 0;
        virtual QGraphicsScene * getScene () = 0;
};

extern Win * global_win;

#endif // WIN_H
