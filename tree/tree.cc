#include "tree.h"
#include "ui_tree.h"

#include <QApplication>
#include <QTextEdit>
#include <QDir>
#include <QDateTime>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tabWidget->clear ();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::displayDir (QTreeWidgetItem * branch, QString path)
{
    QDir dir (path);
    QFileInfoList list = dir.entryInfoList
                         (QDir::AllEntries | QDir::NoDot | QDir::NoDotDot,
                          QDir::DirsFirst | QDir::Name);

    for (QFileInfo info : list)
    {
        QTreeWidgetItem * item = new QTreeWidgetItem;
        QString fullName = info.absoluteFilePath();
        item->setText (0, info.fileName());
        item->setToolTip (0, fullName);
        if (info.isDir())
        {
           item->setForeground (0, QColor ("red"));
           displayDir (item, fullName);
        }
        else
        {
           item->setForeground (0, QColor ("blue"));
           item->setText (1, QString::number (info.size ()));
           item->setText (2, info.lastModified().toString ("yyyy-MM-dd HH:mm:ss"));
           // #include <QDateTime>
        }
        branch->addChild (item);
    }

}

void MainWindow::on_actionRun_triggered()
{
    ui->tree->clear ();
    ui->tree->setHeaderLabels (QStringList () << "name" << "size" << "time");

    QDir dir0 ("../..");
    QDir dir (dir0.absolutePath());

    QTreeWidgetItem * top = new QTreeWidgetItem;
    QString path = dir.absolutePath();
    top->setText (0, dir.dirName());
    top->setToolTip (0, path);
    top->setForeground (0, QColor ("red"));
    ui->tree->addTopLevelItem (top);

    displayDir (top, path);

    ui->tree->expandAll ();
}

void MainWindow::openEditor (QString fileName)
{
    QFileInfo info (fileName);
    QString fullName = info.absoluteFilePath();
    QTextEdit * edit = nullptr;

    if (editors.contains (fullName))
    {
       edit = editors [fullName];
       ui->tabWidget->setCurrentWidget (edit);
    }
    else
    {
        edit = new QTextEdit;
        editors [fullName] = edit;

        QFile f (fileName);
        if (f.open (QFile::ReadOnly))
        {
            QString text = f.readAll ();
            f.close ();
            edit->setText (text);
        }

        QString shortName = info.fileName();
        int inx = ui->tabWidget->addTab (edit, shortName);
        ui->tabWidget->setTabToolTip (inx, fullName);
        ui->tabWidget->setCurrentIndex (inx);
    }
}

void MainWindow::on_tree_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
   QString fileName  = item->toolTip (0);
   openEditor (fileName);
}

void MainWindow::on_actionQuit_triggered()
{
    close ();
}


int main (int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}


