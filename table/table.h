#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

const int MAX = 16;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionRun_triggered();

    void on_actionQuit_triggered();

    void on_slider_sliderMoved(int position);

private:
    Ui::MainWindow *ui;

    int N = 8; // velikost sachovnice
    bool disp; // zobrazovat vysledky
    int results; // pocet reseni
    int a[MAX];
    QColor columnColor (int k);
    void display ();
    void place (int k);

};
#endif // MAINWINDOW_H
