#include "table.h"
#include "ui_table.h"
#include <QApplication>
#include <QTableWidget>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->spinBox->setMinimum (1);
    ui->spinBox->setMaximum (MAX);
    ui->spinBox->setValue (N);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QColor MainWindow::columnColor (int k)
{
    return QColor::fromHsl (k*360/N, 220, 120);
}

void MainWindow::display()
{
    // pridat novou tabulku
    QTableWidget * table = new QTableWidget (this);
    int cnt = ui->tabWidget->count() + 1;
    ui->tabWidget->addTab (table, QString::number (cnt));

    table->setRowCount (N);
    table->setColumnCount (N);

    int w = table->lineWidth();
    for (int i = 0; i < N; i ++)
        table->setColumnWidth (i, w);

    for (int k = 0; k < N; k ++) // k ... cislo sloupce
    {
        int i = a[k]; // i ... cislo radky

        QTableWidgetItem * item = new QTableWidgetItem;

        item->setText (QString::number (k+1));
        item->setToolTip ("policko " + QString::number (i+1) + "," + QString::number (k+1));
        item->setBackgroundColor (columnColor (k));

        table->setItem (i, k, item);
    }
}

/*
template <class T>
class Smart
{
private:
     T * ptr;
public:
     Smart (T * p) { ptr = p; }
     ~Smart () { delete ptr; }
     T  operator * () { return *ptr; }
     T & operator -> () { return *ptr; }
};

void f ()
{
    //int * u = new int;
    Smart <int> u (new int);
    // ....

}
*/



void MainWindow::place(int k) // k ... cislo sloupce
{
    for (int i = 0; i < N; i ++) // i ... cislo radky
    {
        bool ok = true;
        for (int v = 1; v <= k && ok; v ++)
            if (a[k-v] == i || a[k-v] == i-v  || a[k-v] == i+v)
               ok = false;
        if (ok)
        {
            a[k] = i; // v k-em sloupci dama na i-tem radku
            if (k < N-1)
            {
                place (k+1);
            }
            else
            {
                if (disp) display();
                results ++;
            }
        }
    }
}

void MainWindow::on_actionRun_triggered()
{
    N = ui->spinBox->value ();
    disp = ui->checkBox->isChecked();

    ui->tabWidget->clear();
    for (int i = 0; i < N; i ++) a[i] = -1;

    results = 0;
    place (0);

    ui->lineEdit->setText (QString::number (results));
    ui->slider->setMaximum (results-1);
}

void MainWindow::on_slider_sliderMoved(int pos)
{
    if (pos >= 0 && pos < ui->tabWidget->count ())
       ui->tabWidget->setCurrentIndex(pos);
}

void MainWindow::on_actionQuit_triggered()
{
    close ();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

